
export interface Task {
  id?: number, //cuando creamos no venir
  text: string;
  day: string;
  reminder: boolean;
}
