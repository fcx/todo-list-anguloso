import {Task} from "./Task";

export const TASK: Task[] = [
  {
    id: 1,
    text: "Terminar Tarea de angular",
    day: 'Marzo 22',
    reminder: true,
  },
  {
    id: 2,
    text: "Arreglar la silla",
    day: 'Marzo 23',
    reminder: true
  },
  {
    id: 3,
    text: "Terminar de leer foundation",
    day: 'Marzo 25 a las 13:00',
    reminder: false
  },

]
